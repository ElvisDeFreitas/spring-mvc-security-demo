﻿## INTRODUCTION 
Hello, this project have many samples how use **spring security**, like:

1. Authenticate  by forms (JSP) using  default `Authentication Manager`
2. Authenticate by ajax (autentication without json) with default `Authentication Manager`
3. Authenticate with spring security by Integrated `JSON RESTFul` (using spring MVC) service with a custom spring  `Authentication Manager`

## Before everthing
The current project state, cover:

* Authentication by a service (that you can connect to database, but I make it with fixed data for embeddable reasons)
* Authentication by fixed memory spring credentials (usefull if you not use a database or service) 
* I will consider that you are running this application in `http://localhost:8082/spring-mvc-security-demo/`
* I will use `cURL` for make the requests tests
* In the next steps I will use acronyms
	* SS = Spring Security
* Read the Junit Integration tests 

# configure jetty in maven
	https://wiki.eclipse.org/Jetty/Howto/Use_Jetty_with_Maven

# runinng jetty with maven
	https://wiki.eclipse.org/Jetty/Feature/Jetty_Maven_Plugin