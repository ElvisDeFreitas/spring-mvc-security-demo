# PLEASE DON`T FOLLOW THIS FILE, IT IS DEPRECATED!!!
To learn about the project examples please see the junit integration tests


-----------------------------------------------------

## INTRODUCTION 
Hello, this project have many samples how use **spring security**, like:
1. Authenticate  by forms (JSP) using  default `Authentication Manager`
2. Authenticate by ajax (autentication without json) with default `Authentication Manager`
3. Authenticate with spring security by Integrated `JSON RESTFul` (using spring MVC) service with a custom spring  `Authentication Manager`

## Before everthing
The current project state, cover:

* Authentication by a service (that you can connect to database, but I make it with fixed data for embeddable reasons)
* Authentication by fixed memory spring credentials (usefull if you not use a database or service) 
* I will consider that you are running this application in `http://localhost:8080/spring-mvc-security-demo/`
* I will use `cURL` for make the requests tests
* In the next steps I will use acronyms
	* SS = Spring Security

## The project configurations
This project is configured by annotations and programmatically configuration, noting of `XMLs`  , basically the configurations are located in `com.mageddo.server.initializer` and there we have two packages

* initializer package  
	this package is the `web.xml` representation, this are the classes that will be started in the war start, from here we configure everithing or extends it to another classes using `getRootConfigClasses` method, like I have done.
* conf package
	this package save the extended system configurations, here you can setup scanners, beans, etc.
* All spring security configurations are in `src/main/java/com/mageddo/server/conf/SpringSecurityConfig.java` file

## 1 Using spring security by forms (JSP)
Use spring with forms is very simple, this is the `SS` default approach, to test it you can access 

# 1.0
- [OK] configurando o spring security
- [OK] criar o service de login
- [OK] criar controllers que acessam normalmente
- [OK] criar controllers mapeados para serem autenticados
- [OK] criar contrllers de autenticacao
	- [] ver por que o post nao bate no controller de autenticacao
	- [] MAPEAR O REQUEST DE LOGIN EM OUTRO PATTERN PORQUE O /LOGIN EH
			DO SPRING SECURITY PARA LOGAR POR FORM
- [] implementar login por formulario tamb�m
 
# FONTs
* http://www.networkedassets.com/configuring-spring-security-for-a-restful-web-services/

```
curl "http://localhost:8080/spring-mvc-annotation-intercept/form/login" -H "Accept-Encoding: gzip, deflate" -H "Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4" -H "CSP: active" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36" -H "Content-Type: application/x-www-form-urlencoded" -H "Accept: */*" -H "Cookie: JSESSIONID=Sf4hpSIBC9Zl8yUG7V1wX-p_.adminib-i3ep99v" -H "Connection: keep-alive" --data "email=elvis&password=1234" --compressed -i
```

```
curl "http://localhost:8082/spring-mvc-security-demo/form/login" -i  -H "Content-Type: application/x-www-form-urlencoded" --data "email=elvis&password=123" --compressed
```	
	
	-H "Cookie: JSESSIONID=Sf4hpSIBC9Zl8yUG7V1wX-p_.adminib-i3ep99v"