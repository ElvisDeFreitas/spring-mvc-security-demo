package com.mageddo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mageddo.tests.core.Properties;

/**
 *
 * @author deFreitas edigitalb@gmail.com
 */

public class WelcomeControllerIT {
	
	private WelcomeController controller = new WelcomeController();
	private static final Logger logger = LoggerFactory.getLogger(WelcomeControllerIT.class);
	
	@Test
	public void indexTest() throws MalformedURLException, IOException{
		BufferedReader bf = new BufferedReader(
			new InputStreamReader(
				new URL(Properties.BASE_PATH + "/")
				.openConnection().getInputStream()
			)
		);
		
		StringBuilder sb = new StringBuilder();
		String buffer;
		while((buffer = bf.readLine()) != null){
			sb.append(buffer);
		}
		logger.debug("a request retornou:\n {} -------------------------------------\n\n", sb.toString());
		Assert.assertEquals(controller.index(), sb.toString());
		bf.close();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void helloWorldTest() throws MalformedURLException, IOException {
		
		BufferedReader bf = new BufferedReader(
				new InputStreamReader(
						new URL(Properties.BASE_PATH + "/mageddo/helloWorld")
						.openConnection().getInputStream()
						)
				);
		
		StringBuilder sb = new StringBuilder();
		String buffer;
		while((buffer = bf.readLine()) != null){
			sb.append(buffer);
			sb.append(System.lineSeparator());
		}
		
		logger.debug("a request retornou {} -------------------------------------\n\n", sb.toString());
		List<String> messages = (List<String>) controller.helloWorld().getModelMap().get(WelcomeController.MG_MSG);
		Assert.assertTrue(0 <= sb.toString().indexOf(messages.get(0)));
		bf.close();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void fruitsTest() throws MalformedURLException, IOException {
		
		BufferedReader bf = new BufferedReader(
				new InputStreamReader(
						new URL(Properties.BASE_PATH + "/mageddo/fruits")
						.openConnection().getInputStream()
						)
				);
		
		StringBuilder sb = new StringBuilder();
		String buffer;
		while((buffer = bf.readLine()) != null){
			sb.append(buffer);
			sb.append(System.lineSeparator());
		}
		
		logger.debug("a request retornou {} -------------------------------------\n\n", sb.toString());
		List<String> messages = (List<String>) controller.frutis().getModelMap().get(WelcomeController.MG_MSG);
		Assert.assertTrue(0 <= sb.toString().indexOf(messages.get(0)));
		bf.close();
	}
	
}