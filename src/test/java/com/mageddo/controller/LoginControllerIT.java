package com.mageddo.controller;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import com.mageddo.tests.core.Properties;

public class LoginControllerIT {

	public static final String POST_METHOD = "POST";
	public static final String COOKIE_FIELD = "Cookie";
	public static final String SET_COOKIE_FIELD = "Set-Cookie";
	
	@Test
	public void loginSuccessfullTest() throws MalformedURLException, IOException{
		getAuthenticatedCookie();
	}
	
	@Test
	public void loginAuthenticationErrorTest() throws MalformedURLException, IOException{
		final HttpURLConnection con = (HttpURLConnection) new URL(Properties.BASE_PATH + "/login")
		.openConnection();
		
		con.setRequestMethod(POST_METHOD);
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes("username=elvis&password=321");
		wr.flush();
		wr.close();
		
		Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(), con.getResponseCode());
	}
	
	@Test
	public void loginLogoutTest() throws MalformedURLException, IOException {
		getAuthenticatedCookie();
		 HttpURLConnection con = (HttpURLConnection) new URL(Properties.BASE_PATH + "/logout")
		.openConnection();
		con.setRequestMethod(POST_METHOD);
		Assert.assertEquals(HttpStatus.OK.value(), con.getResponseCode());
	}
	
	/**
	 * Do the autentication
	 * @return the authenticated cookie
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static String getAuthenticatedCookie() throws MalformedURLException, IOException{
		final HttpURLConnection con = (HttpURLConnection) new URL(Properties.BASE_PATH + "/login")
		.openConnection();
		
		con.setRequestMethod(POST_METHOD);
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes("username=elvis&password=123");
		wr.flush();
		wr.close();
		
		Assert.assertEquals(HttpStatus.OK.value(), con.getResponseCode());
		Assert.assertNotNull(con.getHeaderField(SET_COOKIE_FIELD));
		Assert.assertFalse(con.getHeaderField(SET_COOKIE_FIELD).isEmpty());
		return con.getHeaderField(SET_COOKIE_FIELD);
	}
	
}
