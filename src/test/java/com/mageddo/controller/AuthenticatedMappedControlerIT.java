package com.mageddo.controller;

import static com.mageddo.controller.LoginControllerIT.COOKIE_FIELD;
import static com.mageddo.controller.LoginControllerIT.getAuthenticatedCookie;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mageddo.json.unit.mocker.PlainDB;
import com.mageddo.tests.core.Properties;

public class AuthenticatedMappedControlerIT {
	private static final Logger logger = LoggerFactory.getLogger(AuthenticatedMappedControler.class);
	private static final String ADMIN_PAGE_ACCESS_DENIED = "/mocks/adminPageAccessDenied.html";
	private static final String ADMIN_PAGE_SUCCESSFULL_REQUEST = "/mocks/adminPageSucessfullRequest.html";
	
	@Test
	public void adminPageErrorTest() throws MalformedURLException, IOException{
		
		HttpURLConnection con = (HttpURLConnection) new URL(Properties.BASE_PATH + "/secure/admin")
			.openConnection();
		// opening connection
		final BufferedReader bf = new BufferedReader(
			new InputStreamReader(
				con.getInputStream()
			)
		);
		
		// reading to byte array
		final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		int b;
		while((b = bf.read()) != -1){
			bytes.write(b);
		}
		
		final byte[] expectedResult = PlainDB.readAsString(ADMIN_PAGE_ACCESS_DENIED).getBytes();
		final byte[] returnedBytes = bytes.toByteArray();
		
		logger.debug(
			"the request returns length is: {}, the expected result length is: {} ",
			returnedBytes.length, expectedResult.length
		);
		
		Arrays.sort(expectedResult); Arrays.sort(returnedBytes);
		Assert.assertTrue(Arrays.equals(expectedResult, returnedBytes));
		
		bf.close();
		bytes.close();
	}
	
	@Test
	public void adminPageSuccessfullTest() throws MalformedURLException, IOException {
		
		HttpURLConnection con = (HttpURLConnection) new URL(Properties.BASE_PATH + "/secure/admin")
			.openConnection();
		con.setRequestProperty(COOKIE_FIELD, getAuthenticatedCookie());
		
		// opening connection
		final BufferedReader bf = new BufferedReader(
				new InputStreamReader(
						con.getInputStream()
						)
				);
		
		// reading to byte array
		final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		int b;
		while((b = bf.read()) != -1){
			bytes.write(b);
		}
		
		final byte[] expectedResult = PlainDB.readAsString(ADMIN_PAGE_SUCCESSFULL_REQUEST).getBytes();
		final byte[] returnedBytes = bytes.toByteArray();
		
		logger.debug(
				"the request returns length is: {}, the expected result length is: {} ",
				returnedBytes.length, expectedResult.length
				);
		System.out.println(new String(returnedBytes));		
		Arrays.sort(expectedResult); Arrays.sort(returnedBytes);
		Assert.assertTrue(Arrays.equals(expectedResult, returnedBytes));
		
		bf.close();
		bytes.close();
	}
}
