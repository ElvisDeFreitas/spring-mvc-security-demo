package com.mageddo.controller.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * Service que autentica o usuário
 * @author elvis
 *
 */
@Component
public class MgAuthenticationProviderImpl extends AbstractUserDetailsAuthenticationProvider {
 
//    @Qualifier("userDetailsService")
	@Autowired
    private UserDetailsService userDetailsService;
     
//  @SuppressWarnings("serial")
    @Override
    protected void additionalAuthenticationChecks(
        UserDetails userDetails,
        UsernamePasswordAuthenticationToken authentication
    ) throws AuthenticationException {
         
//      if(userDetails == null){
//          throw new AuthenticationException("Email inválido"){};
//      }
        if( 
            authentication.getCredentials() == null ||
            !authentication.getCredentials().equals(userDetails.getPassword())){
            throw new BadCredentialsException("Senha inválida");
        }   
    }
 
    @Override
    protected UserDetails retrieveUser(
        String email, UsernamePasswordAuthenticationToken authentication
    ) throws AuthenticationException {
         
        return userDetailsService.loadUserByUsername(email);
    }
     
}