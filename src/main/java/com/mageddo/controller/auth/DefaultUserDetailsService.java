package com.mageddo.controller.auth;

import java.util.Arrays;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class DefaultUserDetailsService implements UserDetailsService {


	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		if(username.equals("elvis")){
			return new User("elvis", "123",true, true, true, true, 
					Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))
			);
		}else if(username.equals("mkyong")){
			return new User("mkyong", "123",true, true, true, true, 
					Arrays.asList(new SimpleGrantedAuthority("USER"))
			);
		}
    throw new UsernameNotFoundException("user not found");
	}

}
