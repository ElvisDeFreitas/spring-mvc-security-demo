package com.mageddo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {
	
	public static final String MG_MSG = "mgMsg";
	
	@RequestMapping("/")
	public @ResponseBody String index(){
		return "<h1>It Works!!!</h1>";
	}
	
	@RequestMapping(value = "/mageddo/helloWorld")
	public ModelAndView helloWorld(){		
		
		List<String> messges = new ArrayList<String>();
		messges.add("Hello World - Hello for you!!");
		return new ModelAndView("helloWorld")
			.addObject(MG_MSG, messges);
	}
	
	@RequestMapping("/mageddo/fruits")
	public ModelAndView frutis(){
		List<String> fruits = new ArrayList<String>();
		fruits.add("Apple");
		return new ModelAndView("fruits")
			.addObject(MG_MSG, fruits);
	}
}
