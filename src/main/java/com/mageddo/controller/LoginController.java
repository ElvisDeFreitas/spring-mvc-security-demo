package com.mageddo.controller;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

	@Inject
  private AuthenticationProvider authenticationManager;
	
  /**
   * Login geral do sistema
   * @param email
   * @param password
   * @return
   */
  @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
  public @ResponseBody Object login(
      @RequestParam("username") String email,
      @RequestParam("password") String password
	) {
       
      try {
          UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
              email, password
          );
          Authentication auth = authenticationManager.authenticate(token);
          SecurityContextHolder.getContext().setAuthentication(auth);
          return new ResponseEntity(HttpStatus.OK);
               
      } catch (Exception e) {
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
      }
  }
  
  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public @ResponseBody Object notLogger() {
		return new ResponseEntity("please login before", HttpStatus.OK);
  }
	
  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/loginFail", method = RequestMethod.GET)
  public @ResponseBody Object loginFail() {
  	return new ResponseEntity("your pass bad credentials", HttpStatus.OK);
  }
  @SuppressWarnings("rawtypes")
  @RequestMapping(value = "/logout", method = RequestMethod.GET)
  public @ResponseBody Object logout() {
  	return new ResponseEntity("your user has been logged off", HttpStatus.OK);
  }
  
}
