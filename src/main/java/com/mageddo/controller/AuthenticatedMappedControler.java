package com.mageddo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/secure")
public class AuthenticatedMappedControler {

	
	@RequestMapping(value = "/admin/**", method = RequestMethod.GET)
	public ModelAndView adminPage(){
		ModelAndView model = new ModelAndView("admin");
		model.addObject("title", "Spring Security Example");
		model.addObject("message", "This page is for ROLE_ADMIN only!");
		return model;
	}
	
}
