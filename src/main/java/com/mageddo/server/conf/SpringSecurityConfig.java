package com.mageddo.server.conf;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
 
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
 
//	 UNCOMMENT IT if you have been create a UserDetailService implementation
//    @Qualifier("userDetailsService")
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * This method will be called when the system login by the mapped urls in
     *  {@link #configureAuthenthication(HttpSecurity)}
     *  <br>
     *  To login by a custom url you can use like LoginController#login
     * @param auth
     * @throws Exception
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	/*
    	 * If you save the user with a password critptograffer, you can use the follow synthax
    	 * 
    	 */
//    	  auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    	 
    	/*
    	 * To login without a criptograffer you use as follow 
    	 */
        auth.userDetailsService(userDetailsService);
    	
      	/*
      	 * To login with a fixed users, without a service or database you can use as follow
      	 */
//    	 auth.inMemoryAuthentication().withUser("mkyong").password("123456").roles("USER");
//   	  auth.inMemoryAuthentication().withUser("admin").password("123456").roles("ADMIN");
//   	  auth.inMemoryAuthentication().withUser("dba").password("123456").roles("DBA");
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
      configureAuthenthication(http);
    }
 
    /**
     * Configura a autenticação
     * @param http 
     * @throws Exception 
     */
    private void configureAuthenthication(HttpSecurity http) throws Exception {
//      desabilita a prevenção de injeção para poder fazer requisições post e CORS pelo webservice
        http.csrf().disable().authorizeRequests()
         
        // parte livre
//      .antMatchers(HttpMethod.POST, "/**").permitAll()

//      the next url pattern will be authenticated with the ROLE_ADMIN
        .antMatchers("/secure/**")
            .access("hasRole('ROLE_ADMIN')")
            
//            with the next settings spring will create a login form, login fail and logout 
//
            .and().formLogin()
            
//            FORM TO LOGIN IN SPRING (if not specified spring use a default)
            .loginPage("/form/login")
	            .usernameParameter("username")
	            .passwordParameter("password")
	            
//	            login for default process the login (you can make de authentication by curl)
            .loginProcessingUrl("/form/login")
            	.usernameParameter("username")
            	.passwordParameter("password")
            	
//            	the validation url
            .failureUrl("/form/login?error")
                .and().logout()
                
//                the url to logout in the system (by default is the /logout)
//                .logoutUrl(logoutUrl)
                
//                the url where the system will go when logout sucessfull
                .logoutSuccessUrl("/form/login?logout")
                
//                the url when the system has been logged but have not permission
                .and().exceptionHandling().accessDeniedPage("/403");
 
    }
    
 
    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
     
}