package com.mageddo.server.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.mageddo.interceptor.FruitsInterceptor;
import com.mageddo.interceptor.MainInterceptor;

@ComponentScan(
				basePackages = {"com.*"},
				excludeFilters = {
					@ComponentScan.Filter(
								type = FilterType.ANNOTATION, value = Configuration.class
					)
				}
)
@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {

	 @Bean
   public InternalResourceViewResolver viewResolver() {
       InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
       viewResolver.setViewClass(JstlView.class);
       viewResolver.setPrefix("/WEB-INF/pages/");
       viewResolver.setSuffix(".jsp");
       return viewResolver;
   }
	 
	 @Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new MainInterceptor()).addPathPatterns("/**");
		registry.addInterceptor(new FruitsInterceptor()).addPathPatterns("/mageddo/fruits");
		
	}
	 

	 
	 
	
}