package com.mageddo.server.initializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.mageddo.server.conf.AppConfig;
import com.mageddo.server.conf.SpringSecurityConfig;
import com.mageddo.server.conf.SpringMvcConf;

public class SpringMvcInitializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {
	
	
	// private static Logger log = Logger.getLogger(SpringMvcInitializer.class);

	/**
	 * Registry the default configurations
	 * 
	 * @param servletContext
	 * @throws ServletException
	 */
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
	}

	@Override
	protected WebApplicationContext createServletApplicationContext() {
		WebApplicationContext context = super.createServletApplicationContext();
		return context;
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { AppConfig.class, SpringMvcConf.class, SpringSecurityConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	

}