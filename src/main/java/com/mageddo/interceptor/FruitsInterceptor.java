package com.mageddo.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import static com.mageddo.controller.WelcomeController.MG_MSG;

public class FruitsInterceptor extends HandlerInterceptorAdapter{



	//before the actual handler will be executed
	@Override
	public boolean preHandle(HttpServletRequest request, 
			HttpServletResponse response, Object handler)
	    throws Exception {
	
	
		// follow request?
		return true;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
		if(modelAndView == null)return ;
		List<String> msgs =  (List<String>) modelAndView.getModelMap().get(MG_MSG);
		if(msgs == null){
			msgs =  new ArrayList<String>();
			modelAndView.addObject(MG_MSG, new ArrayList<String>());
		}
		msgs.add("Fruits Interceptor - Grape!!! ");
	}

}