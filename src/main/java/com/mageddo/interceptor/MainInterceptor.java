package com.mageddo.interceptor;

import java.util.ArrayList;
import java.util.List;

//import javax.interceptor.Interceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import static com.mageddo.controller.WelcomeController.MG_MSG;

//@Interceptor
public class MainInterceptor extends HandlerInterceptorAdapter {
	
	private static final Logger logger = Logger.getLogger(MainInterceptor.class);

	//before the actual handler will be executed
	@Override
	public boolean preHandle(HttpServletRequest request, 
			HttpServletResponse response, Object handler)
	    throws Exception {
//			((HandlerMethod)handler).getMethodAnnotation(annotationType)
		
		logger.warn("before execute in mapped method, I was executed");
		
		// follow request?
		return true;
	}

	//after the handler is executed
	@Override
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, 
			Object handler, ModelAndView modelAndView)
			throws Exception {
		logger.warn("after execute in mapped method, I was executed");
		if(modelAndView == null)return ;
		List<String> msgs =  (List<String>) modelAndView.getModelMap().get(MG_MSG);
		if(msgs == null){
			msgs =  new ArrayList<String>();
			modelAndView.addObject(MG_MSG, new ArrayList<String>());
		}
		msgs.add("Main Interceptor - Hello!!! ");
	}
}